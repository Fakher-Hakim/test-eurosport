package com.fakher.eurosport.data.local

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.runner.AndroidJUnit4
import com.fakher.eurosport.data.model.Article
import com.fakher.eurosport.data.model.ArticleType
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainDatabaseTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var articleDao: ArticleDao
    private lateinit var db: MainDatabase

    @Before
    fun setUp() {
        val context: Context = ApplicationProvider.getApplicationContext()
        // Using an in-memory database because the information stored here disappears when the
        // process is killed.
        db = Room.inMemoryDatabaseBuilder(context, MainDatabase::class.java)
            // Allowing main thread queries, just for testing.
            .allowMainThreadQueries()
            .build()
        articleDao = db.articleDao()
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun getAllArticles() {
        val article =
            Article(
                1,
                "title 1",
                "description 1",
                "fake.com",
                "author 1",
                125240000L,
                "FOOTBALL",
                12,
                ArticleType.VIDEO,
                "fake.com"
            )

        runBlocking {
            articleDao.insertArticles(listOf(article))

            val articles = articleDao.getArticles()
            assertEquals(articles, listOf(article))
        }
    }
}