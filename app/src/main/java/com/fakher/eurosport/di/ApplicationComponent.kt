package com.fakher.eurosport.di

import com.fakher.eurosport.data.local.DaoModule
import com.fakher.eurosport.data.repository.RepositoryModule
import com.fakher.eurosport.ui.home.HomeViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, RepositoryModule::class, DaoModule::class])
interface ApplicationComponent {

    fun getHomeVMFactory(): ViewModelFactory<HomeViewModel>
}