package com.fakher.eurosport.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fakher.eurosport.R
import com.fakher.eurosport.data.model.Article
import com.fakher.eurosport.data.model.ArticleType
import com.fakher.eurosport.timeAgo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_article.view.*
import kotlinx.android.synthetic.main.item_video.view.*
import kotlinx.android.synthetic.main.item_video.view.article_label
import kotlinx.android.synthetic.main.item_video.view.item_image_holder
import kotlinx.android.synthetic.main.item_video.view.view_title

class ArticleAdapter(private val listener: OnArticleClickedListener) :
    RecyclerView.Adapter<ArticleAdapter.ArticleItemViewHolder>() {

    private val articles = mutableListOf<Article>()

    fun addArticles(newArticles: List<Article>) {
        articles.clear()
        articles.addAll(newArticles)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ArticleItemViewHolder {
        val view: View = if (viewType == TYPE_ARTICLE) {
            LayoutInflater.from(parent.context).inflate(R.layout.item_article, parent, false)
        } else {
            LayoutInflater.from(parent.context).inflate(R.layout.item_video, parent, false)
        }

        return ArticleItemViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return if (articles[position].type == ArticleType.ARTICLE) {
            TYPE_ARTICLE
        } else {
            TYPE_VIDEO
        }
    }

    override fun onBindViewHolder(holder: ArticleItemViewHolder, position: Int) {
        holder.bind(articles[position], getItemViewType(position), listener)
    }

    override fun getItemCount() = articles.size

    class ArticleItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val coverImageView = itemView.item_image_holder
        private val titleView = itemView.view_title
        private val articleLabelTextView = itemView.article_label

        private val authorTextView = itemView.article_author
        private val dateTextView = itemView.article_date
        private val viewsTextView = itemView.video_views

        fun bind(item: Article, itemViewType: Int, listener: OnArticleClickedListener) {

            Picasso.get().load(item.imageURL).into(coverImageView)
            titleView.text = item.title
            articleLabelTextView.text = item.label

            if (itemViewType == TYPE_ARTICLE) {
                dateTextView.text = item.date.timeAgo()
                if (item.author.isEmpty()) {
                    authorTextView.visibility = GONE
                    itemView.separator_text_view.visibility = GONE
                } else {
                    authorTextView.text = item.author
                }
            } else {
                viewsTextView.text = item.videoViewsCount.toString()
            }

            itemView.setOnClickListener {
                listener.onArticleClicked(item)
            }
        }
    }

    interface OnArticleClickedListener {
        fun onArticleClicked(article: Article)
    }

    companion object {
        private const val TYPE_ARTICLE = 1
        private const val TYPE_VIDEO = 2
    }
}
