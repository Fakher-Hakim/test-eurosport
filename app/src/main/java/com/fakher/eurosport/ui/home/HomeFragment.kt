package com.fakher.eurosport.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.fakher.eurosport.R
import com.fakher.eurosport.data.model.Article
import com.fakher.eurosport.data.model.ArticleType
import com.fakher.eurosport.di.injector
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment(), ArticleAdapter.OnArticleClickedListener {

    private val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(this, activity?.injector?.getHomeVMFactory())
            .get(HomeViewModel::class.java)
    }

    private lateinit var articleAdapter: ArticleAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.e("TAG", "onViewCreated")
        initView()
        observeViewModel()
        viewModel.getArticles()
    }

    override fun onStart() {
        super.onStart()
        Log.e("TAG", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.e("TAG", "onResume")
    }

    private fun observeViewModel() {
        viewModel.articles.observe(this, Observer {
            if (it.isNullOrEmpty()) {
                article_rv.visibility = View.GONE
                no_article_view.visibility = View.VISIBLE
            } else {
                article_rv.visibility = View.VISIBLE
                no_article_view.visibility = View.GONE
                articleAdapter.addArticles(it)
            }
        })

        viewModel.loading.observe(this, Observer { isLoading ->
            progress_bar.visibility = if (isLoading) View.VISIBLE else View.GONE
        })

        viewModel.errorMessage.observe(this, Observer { message ->
            message?.let {
                Snackbar.make(no_article_view, message, Snackbar.LENGTH_SHORT).show()
                viewModel.onSnackbarShown()
            }
        })
    }

    private fun initView() {
        articleAdapter = ArticleAdapter(this)
        article_rv.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = articleAdapter
        }
    }

    override fun onArticleClicked(article: Article) {
        var action: NavDirections
        if (article.type == ArticleType.ARTICLE) {
            action = HomeFragmentDirections.actionHomeToArticle(article)
        } else {
            action = HomeFragmentDirections.actionHomeToVideo(article)
        }
        Navigation.findNavController(article_rv).navigate(action)
    }
}