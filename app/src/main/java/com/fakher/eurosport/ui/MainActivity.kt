package com.fakher.eurosport.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.fakher.eurosport.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
