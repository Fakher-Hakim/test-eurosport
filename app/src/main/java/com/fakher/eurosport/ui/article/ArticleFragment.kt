package com.fakher.eurosport.ui.article

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.fakher.eurosport.R
import com.fakher.eurosport.timeAgo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_article_detail.*

class ArticleFragment : Fragment() {

    private val args: ArticleFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_article_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val article = args.article

        article_title.text = article.title
        article_time.text = article.date.timeAgo()
        article_description.text = article.description
        if (article.author.isEmpty()) {
            by_text.visibility = View.INVISIBLE
        } else {
            article_author.text = article.author
        }

        Picasso.get().load(article.imageURL).into(cover_image)

        back_button.setOnClickListener {
            val action = ArticleFragmentDirections.actionBackHome()
            Navigation.findNavController(article_title).navigate(action)
        }

        share_button.setOnClickListener {
            Log.i(ArticleFragment::class.java.simpleName, "share button clicked!")
        }
    }
}