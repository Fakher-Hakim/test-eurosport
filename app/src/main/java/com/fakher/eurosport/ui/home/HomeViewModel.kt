package com.fakher.eurosport.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fakher.eurosport.data.model.Article
import com.fakher.eurosport.data.repository.ArticleRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val articleRepository: ArticleRepository) :
    ViewModel() {

    private val _articles: MutableLiveData<List<Article>> = MutableLiveData()
    val articles: LiveData<List<Article>>
        get() = _articles

    private val _loading: MutableLiveData<Boolean> = MutableLiveData()
    val loading: LiveData<Boolean>
        get() = _loading

    private val _errorMessage: MutableLiveData<String?> = MutableLiveData(null)
    val errorMessage: LiveData<String?>
        get() = _errorMessage

    fun getArticles() {
        viewModelScope.launch {
            _loading.value = true
            try {
                _articles.value = articleRepository.getArticles()
            } catch (e: Exception) {
                _errorMessage.value = "Error: ${e.message}"
            } finally {
                _articles.value = articleRepository.getArticles(false)
                _loading.value = false
            }
        }
    }

    fun onSnackbarShown() {
        _errorMessage.value = null
    }
}