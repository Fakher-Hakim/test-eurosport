package com.fakher.eurosport.data.repository

import com.fakher.eurosport.data.local.ArticleDao
import com.fakher.eurosport.data.remote.NetworkApi
import com.fakher.eurosport.data.remote.NetworkModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [NetworkModule::class])
class RepositoryModule {

    @Provides
    @Singleton
    fun provideTaskRepository(articleDao: ArticleDao, networkApi: NetworkApi): ArticleRepository {
        return ArticleRepository(networkApi, articleDao)
    }
}