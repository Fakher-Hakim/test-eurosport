package com.fakher.eurosport.data.remote

import com.fakher.eurosport.data.model.ArticleListResponse
import retrofit2.http.GET

interface ApiService {

    @GET("findstories.json?c=-1%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C&l=3&p=mob&m=50&o=editorial&isSonic=1/")
    suspend fun getArticles(): ArticleListResponse
}