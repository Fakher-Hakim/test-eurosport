package com.fakher.eurosport.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "ARTICLES")
@Parcelize
data class Article(
    @PrimaryKey
    val id: Int,
    @SerializedName("seotitle") val title: String,
    val description: String,
    val imageURL: String,
    val author: String,
    val date: Long,
    @SerializedName("agency") val label: String,
    val videoViewsCount: Int,
    val type: ArticleType,
    val videoURL: String
) : Parcelable