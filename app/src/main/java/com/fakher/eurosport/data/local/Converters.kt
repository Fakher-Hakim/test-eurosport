package com.fakher.eurosport.data.local

import androidx.room.TypeConverter
import com.fakher.eurosport.data.model.ArticleType

class Converters {

    @TypeConverter
    fun typeToString(value: ArticleType): String {
        return value.persistenceValue
    }

    @TypeConverter
    fun intToType(persistenceValue: String): ArticleType {
        return ArticleType.valueOf(persistenceValue)
    }
}