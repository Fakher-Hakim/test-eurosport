package com.fakher.eurosport.data.repository

import com.fakher.eurosport.data.local.ArticleDao
import com.fakher.eurosport.data.model.Article
import com.fakher.eurosport.data.remote.NetworkApi
import javax.inject.Inject

class ArticleRepository @Inject constructor(
    private val networkApi: NetworkApi,
    private val articleDao: ArticleDao
) {

    suspend fun getArticles(forceUpdate: Boolean = true): List<Article> {
        if (forceUpdate) {
            val articles = networkApi.getArticles()
            if (!articles.isNullOrEmpty()) {
                articleDao.insertArticles(articles)
            }
        }

        return articleDao.getArticles().sortedByDescending { it.date }
    }
}