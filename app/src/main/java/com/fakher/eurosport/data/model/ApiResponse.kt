package com.fakher.eurosport.data.model

import com.google.gson.annotations.SerializedName

data class ArticleListResponse(val stories: List<ArticleResponse>)

data class ArticleResponse(

    val author: String?,

    val media: MediaResponse,

    @SerializedName("featureddate")
    val featuredDate: Float,

    @SerializedName("passthrough")
    var passThrough: PassthroughResponse?,

    @SerializedName("storyagency")
    val storyAgency: StoryagencyResponse,

    @SerializedName("teaser")
    val teaser: String,

    @SerializedName("title")
    val title: String,

    @SerializedName("id")
    val id: Int
)

data class PassthroughResponse(@SerializedName("content") var content: ContentResponse?)

class ContentResponse(@SerializedName("views") var views: Int)

data class StoryagencyResponse(val name: String)

data class MediaResponse(var picture: Picture)

class Picture(
    @SerializedName("pictureurl")
    var pictureurl: String
)

enum class ArticleType(val persistenceValue: String) {
    ARTICLE("ARTICLE"), VIDEO("VIDEO")
}

fun ArticleResponse.toArticle(): Article {

    val date = featuredDate.toLong() * 1000
    val article = Article(
        id,
        title,
        teaser,
        BASE_IMAGE_URL + media.picture.pictureurl,
        author ?: "",
        date,
        storyAgency.name,
        passThrough?.content?.views ?: 0,
        ArticleType.ARTICLE,
        ""
    )
    return article
}

private const val BASE_IMAGE_URL = "https://i.eurosport.com/"
