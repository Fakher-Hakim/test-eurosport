package com.fakher.eurosport.data.local

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DaoModule {

    @Provides
    @Singleton
    fun providesMainDatabase(context: Context): MainDatabase {
        return MainDatabase(context)
    }

    @Provides
    @Singleton
    fun provideTaskDao(mainDatabase: MainDatabase): ArticleDao {
        return mainDatabase.articleDao()
    }
}