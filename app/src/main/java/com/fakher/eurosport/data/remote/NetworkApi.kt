package com.fakher.eurosport.data.remote

import com.fakher.eurosport.data.model.Article
import com.fakher.eurosport.data.model.toArticle
import javax.inject.Inject

class NetworkApi @Inject constructor(private val apiService: ApiService) {

    suspend fun getArticles(): List<Article> {
        return apiService.getArticles().stories.map { it.toArticle() }
    }
}