package com.fakher.eurosport

import android.app.Application
import com.facebook.stetho.Stetho
import com.fakher.eurosport.di.ApplicationComponent
import com.fakher.eurosport.di.ApplicationModule
import com.fakher.eurosport.di.DaggerApplicationComponent
import com.fakher.eurosport.di.DaggerComponentProvider

class MyApplication : Application(), DaggerComponentProvider {

    override val component: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationModule(
                ApplicationModule(
                    this
                )
            )
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }
}