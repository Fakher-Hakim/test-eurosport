package com.fakher.eurosport.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.fakher.eurosport.data.model.Article
import com.fakher.eurosport.data.model.ArticleType
import com.fakher.eurosport.data.repository.ArticleRepository
import com.fakher.multimodule.mvvm.utils.MainCoroutineScopeRule
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class HomeViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineScope = MainCoroutineScopeRule()

    @MockK
    lateinit var articleRepository: ArticleRepository

    val article =
        Article(
            1,
            "title 1",
            "description 1",
            "fake.com",
            "author 1",
            125240000L,
            "TRANSFERT",
            12,
            ArticleType.ARTICLE,
            "fake.com"
        )

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun should_load_articles_successfully() {
        val viewModel = HomeViewModel(articleRepository)

        coEvery { articleRepository.getArticles(any()) } returns listOf(
            article,
            article.copy(id = 2),
            article.copy(id = 3)
        )

        runBlockingTest {
            viewModel.getArticles()

            Assert.assertNull(viewModel.errorMessage.value)
            Assert.assertEquals(
                listOf(article, article.copy(id = 2), article.copy(id = 3)),
                viewModel.articles.value
            )
        }
    }

    @Test
    fun should_load_articles_fail() {
        val viewModel = HomeViewModel(articleRepository)

        coEvery { articleRepository.getArticles(true) } throws Exception("This is error")
        coEvery { articleRepository.getArticles(false) } returns listOf(article)

        runBlockingTest {
            viewModel.getArticles()

            Assert.assertEquals("Error: This is error", viewModel.errorMessage.value)
            Assert.assertEquals(listOf(article), viewModel.articles.value)
        }
    }

    @Test
    fun should_init_snack_bar_error() {
        val viewModel = HomeViewModel(articleRepository)

        runBlockingTest {
            viewModel.onSnackbarShown()

            Assert.assertNull(viewModel.errorMessage.value)
        }
    }
}