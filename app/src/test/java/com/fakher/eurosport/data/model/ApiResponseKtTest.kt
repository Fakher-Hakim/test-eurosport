package com.fakher.eurosport.data.model

import org.junit.Assert
import org.junit.Test

class ApiResponseKtTest {

    val media = MediaResponse(Picture("url"))

    val passThrough = PassthroughResponse(ContentResponse(12983))

    val storyAgency = StoryagencyResponse("agency")

    val articleResponse = ArticleResponse(
        "author",
        media,
        15881246400f,
        passThrough,
        storyAgency,
        "teaser",
        "title",
        12
    )

    val article = Article(
        12,
        "title",
        "teaser",
        "https://i.eurosport.com/url",
        "author",
        15881246720000,
        "agency",
        12983,
        ArticleType.ARTICLE,
        ""
    )

    @Test
    fun should_convert_successful() {
        Assert.assertEquals(articleResponse.toArticle(), article)
    }
}