package com.fakher.eurosport.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.fakher.eurosport.data.local.ArticleDao
import com.fakher.eurosport.data.model.Article
import com.fakher.eurosport.data.model.ArticleType
import com.fakher.eurosport.data.remote.NetworkApi
import com.fakher.multimodule.mvvm.utils.MainCoroutineScopeRule
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ArticleRepositoryTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineScope = MainCoroutineScopeRule()

    @MockK
    private lateinit var articleDao: ArticleDao

    @MockK
    private lateinit var networkApi: NetworkApi

    private lateinit var articleRepository: ArticleRepository

    val article =
        Article(
            1,
            "title 1",
            "description 1",
            "fake.com",
            "author 1",
            125240000L,
            "FOOTBALL",
            12,
            ArticleType.VIDEO,
            "fake.com"
        )


    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        articleRepository = ArticleRepository(networkApi, articleDao)
    }

    @Test
    fun should_get_articles_from_network() {
        coEvery { networkApi.getArticles() } returns listOf(article, article.copy(id = 2))
        coEvery { articleDao.getArticles() } returns listOf(article, article.copy(id = 2))

        runBlockingTest {
            val result = articleRepository.getArticles()

            coVerify { networkApi.getArticles() }
            coVerify { articleDao.insertArticles(listOf(article, article.copy(id = 2))) }

            Assert.assertEquals(listOf(article, article.copy(id = 2)), result)
        }
    }

    @Test
    fun should_get_articles_from_database() {
        coEvery { networkApi.getArticles() } throws Exception("exception")
        coEvery { articleDao.getArticles() } returns listOf(article, article.copy(id = 2))

        runBlockingTest {
            val result = articleRepository.getArticles(false)

            coVerify(exactly = 0) { networkApi.getArticles() }
            coVerify(exactly = 0) { articleDao.insertArticles(any()) }

            Assert.assertEquals(listOf(article, article.copy(id = 2)), result)
        }
    }
}